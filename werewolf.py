import sys
import requests
from g_python.gextension import Extension
from g_python.hmessage import Direction

extension_info = {
    "title": "Werewolf Extended",
    "description": "This might be start of something",
    "version": "0.1",
    "author": "Yarkin"
}

ext = Extension(extension_info, sys.argv, silent=True)
ext.start()

url = "https://raw.githubusercontent.com/yyarkinn/testrepo/main/werewolf.py"

try:
    response = requests.get(url)
    if response.status_code == 200:
        with open("werewolf.py", "w") as file:
            file.write(response.text)
            print("File updated successfully.")
    else:
        print("Failed to fetch data from the URL.")
except Exception as e:
    print("An error occurred:", e)
    
def Room(message):
    enter_room, flat_id, room_name, owner_id, owner_name, door_mode, user_count, max_user_count, description, trade_mode, score, ranking, category_id = message.packet.read("bisisiiisiiii")
    if flat_id == 79270177:
        ext.send_to_client('{in:Users}{i:1}{i:616806038}{s:"Yarkin-BOT"}{s:""}{s:"hr-170-34.hd-180-17.ch-210-1337.lg-280-1427.sh-305-79.ha-1005-1427.he-3274-1427.ca-3292-79"}{i:10000}{i:40}{i:47}{s:"0.0"}{i:2}{i:1}{s:"m"}{i:-1}{i:-1}{i:0}{i:3270}{b:false}')
        ext.send_to_client('{in:Whisper}{i:10000}{s:"Werewolf Extended v0.1"}{i:0}{i:23}{i:0}{i:-1}{i:527}')
        ext.send_to_client('{in:ObjectAdd}{i:837279515}{i:4253}{i:44}{i:52}{i:0}{s:"0.5"}{s:"1.0"}{i:0}{i:2}{i:5}{s:"0"}{s:"581441"}{s:"b22134s11137s20054s551370359e79f946d3343e821f9c8d645510c"}{s:"7d7d7d"}{s:"00539b"}{i:-1}{i:1}{i:66806038}{s:"Yarkin"}')

def Object(message):
    itemid, idk, condition = message.packet.read('sis')
    if itemid == '2147418299':
        print("Number 0 Box has been clicked.")
        ext.send_to_client('{in:Shout}{i:10000}{s:"Number 0 Box has been clicked."}{i:0}{i:23}{i:0}{i:-1}')
    elif itemid == '2147418296':
        print("Number 1 Box has been clicked.")
        ext.send_to_client('{in:Shout}{i:10000}{s:"Number 1 Box has been clicked."}{i:0}{i:23}{i:0}{i:-1}')
    elif itemid == '2147418292':
        print("Number 2 Box has been clicked.")
        ext.send_to_client('{in:Shout}{i:10000}{s:"Number 2 Box has been clicked."}{i:0}{i:23}{i:0}{i:-1}')
    elif itemid == '2147418289':
        print("Number 3 Box has been clicked.")
        ext.send_to_client('{in:Shout}{i:10000}{s:"Number 3 Box has been clicked."}{i:0}{i:23}{i:0}{i:-1}')
    elif itemid == '2147418286':
        print("Number 4 Box has been clicked.")
        ext.send_to_client('{in:Shout}{i:10000}{s:"Number 4 Box has been clicked."}{i:0}{i:23}{i:0}{i:-1}')
    elif itemid == '2147418305':
        print("Number 5 Box has been clicked.")
        ext.send_to_client('{in:Shout}{i:10000}{s:"Number 5 Box has been clicked."}{i:0}{i:23}{i:0}{i:-1}')
    elif itemid == '2147418323':
        print("Number 6 Box has been clicked.")
        ext.send_to_client('{in:Shout}{i:10000}{s:"Number 6 Box has been clicked."}{i:0}{i:23}{i:0}{i:-1}')
    elif itemid == '2147418351':
        print("Number 7 box has been clicked.")
        ext.send_to_client('{in:Shout}{i:10000}{s:"Number 7 Box has been clicked."}{i:0}{i:23}{i:0}{i:-1}')
    elif itemid == '2147418382':
        print("Number 8 box has been clicked.")
        ext.send_to_client('{in:Shout}{i:10000}{s:"Number 8 Box has been clicked."}{i:0}{i:23}{i:0}{i:-1}')
    elif itemid == '2147418412':
        print("Number 9 box has been clicked.")
        ext.send_to_client('{in:Shout}{i:10000}{s:"Number 9 Box has been clicked."}{i:0}{i:23}{i:0}{i:-1}')
    elif itemid == '2147418372':
        print("Skip/Save box has been clicked.")
        ext.send_to_client('{in:Shout}{i:10000}{s:"Skip/Save box has been clicked."}{i:0}{i:23}{i:0}{i:-1}')
    elif itemid == '2147418360':
        print("Mayor box has been clicked.")
        ext.send_to_client('{in:Shout}{i:10000}{s:"Mayor box has been clicked."}{i:0}{i:23}{i:0}{i:-1}')
    elif itemid == '2147419125':
        print("Wolwes disagree box has been clicked.")
        ext.send_to_client('{in:Shout}{i:10000}{s:"Wolwes disagree box has been clicked."}{i:0}{i:23}{i:0}{i:-1}')
    elif itemid == '2147419126':
        print("Wolwes agree box has been clicked.")
        ext.send_to_client('{in:Shout}{i:10000}{s:"Wolwes agree box has been clicked."}{i:0}{i:23}{i:0}{i:-1}')    
    else:
        pass

ext.intercept(Direction.TO_CLIENT, Object, 'ObjectDataUpdate')
ext.intercept(Direction.TO_CLIENT, Room, "GetGuestRoomResult")